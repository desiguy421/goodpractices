//
//  NSObjectTests.m
//  GoodPractices
//
//  Created by Aqeel Gunja on 2/6/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <objc/runtime.h>
#import "TestHelper.h"

@interface NSObjectTests : XCTestCase

@property (nonatomic, strong) NSArray *projectClasses;


@end

@implementation NSObjectTests

- (void)populateProjectClasses
{
    NSArray *projectClasses = nil;
    [TestHelper populateProjectClassList:&projectClasses withClassFilterBlock:NULL];
    self.projectClasses = projectClasses;
}

- (void)setUp
{
    [super setUp];
    [self populateProjectClasses];
}

- (void)testAllClassesWithBlock:(TestBlock)testBlock
{
    for (Class class in self.projectClasses)
    {
        testBlock(class);
    }
}

- (void)testSuperIsCalledInAwakeFromNib
{
    [self testAllClassesWithBlock:^(__unsafe_unretained Class class)
    {
        BOOL didCallSuper = [TestHelper isSuperCalledForSelector:@selector(awakeFromNib) inClass:class];
        XCTAssert(didCallSuper, @"[%@ awakeFromNib] does not call super.", NSStringFromClass(class));
    }];
}

- (void)testAllNSStringReadWritePropertiesAreCopied
{
    [self testAllClassesWithBlock:^(__unsafe_unretained Class class)
    {
        unsigned int outCount = 0;
        objc_property_t *propertyList = class_copyPropertyList(class, &outCount);
        for (int i = 0; i < outCount; i++)
        {
            objc_property_t property = propertyList[i];
            const char *propertyAttributes = property_getAttributes(property);
            NSString *attributesString = [NSString stringWithCString:propertyAttributes encoding:NSUTF8StringEncoding];

            BOOL containsCopy = NO;
            NSRange range = [attributesString rangeOfString:@"NSString"];
            if (range.location != NSNotFound)
            {
                range = [attributesString rangeOfString:@",C"];
                NSRange readOnlyRange = [attributesString rangeOfString:@",R"];
                if (readOnlyRange.location == NSNotFound)
                {
                    if (range.location != NSNotFound)
                    {
                        containsCopy = YES;
                    }
                }
                else
                {
                    continue;
                }
            }
            else
            {
                continue;
            }
            XCTAssert(containsCopy, @"%@ has NSString properties that are not copied.", NSStringFromClass(class));
        }
        free(propertyList);
    }];
}

- (void)tearDown
{
    [super tearDown];
}



@end
