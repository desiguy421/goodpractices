//
//  UIViewTests.m
//  GoodPractices
//
//  Created by Aqeel Gunja on 2/6/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <objc/runtime.h>
#import "TestHelper.h"

@interface UIViewTests : XCTestCase

@property (nonatomic, strong) NSArray *projectViewClasses;

@end

@implementation UIViewTests

- (void)populateProjectViewClassList
{
    NSArray *classes = nil;
    [TestHelper populateProjectClassList:&classes withClassFilterBlock:^BOOL(__unsafe_unretained Class projectClass)
    {
        return ([projectClass isSubclassOfClass:[UIView class]]);
    }];
    self.projectViewClasses = classes;
}

- (void)setUp
{
    [super setUp];
    [self populateProjectViewClassList];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testAllViewClassesWithBlock:(TestBlock)testBlock
{
    for (Class class in self.projectViewClasses)
    {
        testBlock(class);
    }
}

- (void)testSuperIsCalledInLayoutSubviews
{
    [self testAllViewClassesWithBlock:^(__unsafe_unretained Class class)
    {
        BOOL didCallSuper = [TestHelper isSuperCalledForSelector:@selector(layoutSubviews) inClass:class];
        XCTAssert(didCallSuper, @"[%@ layoutSubviews] did not call super.", NSStringFromClass(class));
    }];
}

@end
