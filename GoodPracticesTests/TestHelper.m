//
//  TestHelper.m
//  GoodPractices
//
//  Created by Aqeel Gunja on 1/20/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import "TestHelper.h"

@implementation TestHelper

+ (void)populateProjectClassList:(NSArray **)classArray withClassFilterBlock:(ClassFilterBlock)classFilterBlock
{
    NSMutableArray *classes = [[NSMutableArray alloc] init];
    unsigned int count = 0;
    Class *classList = objc_copyClassList(&count);
    NSBundle *bundle = [NSBundle mainBundle];
    for (int i = 0; i < count; i++)
    {
        Class class = classList[i];
        Class superClass = class_getSuperclass(class);
        if (superClass
            && [NSBundle bundleForClass:class] == bundle
            && (!classFilterBlock || classFilterBlock(class)))
        {
            [classes addObject:class];
        }
    }
    free(classList);
    *classArray = [classes copy];
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
+ (BOOL)isSuperCalledForSelector:(SEL)selector inClass:(Class)class
{
    Class superClass = class_getSuperclass(class);
    if (method_getImplementation(class_getInstanceMethod(class, selector)) ==
        method_getImplementation(class_getInstanceMethod(superClass, selector)))
    {
        return YES;
    }

    NSString *mockSelectorString = [NSString stringWithFormat:@"mock_%@", NSStringFromSelector(selector)];

    SEL mockSelector = NSSelectorFromString(mockSelectorString);
    Method method = class_getInstanceMethod(superClass, selector);
    const char *types = method_getTypeEncoding(method);

    IMP imp = imp_implementationWithBlock(^(id selfReference)
                                          {
                                              NSNumber *yesNumber = @(YES);
                                              objc_setAssociatedObject(selfReference, (__bridge const void *)(mockSelectorString), yesNumber, OBJC_ASSOCIATION_RETAIN);

                                              [selfReference performSelector:mockSelector withObject:nil];

                                          });
    class_addMethod(superClass, NSSelectorFromString(mockSelectorString), imp, types);
    Method mockMethod = class_getInstanceMethod(superClass, mockSelector);
    method_exchangeImplementations(method, mockMethod);

    id object = [[class alloc] init];
    [object performSelector:selector withObject:nil];

    NSNumber *number = objc_getAssociatedObject(object, (__bridge const void *)(mockSelectorString));
    BOOL didCallSuper = [number boolValue];

    return didCallSuper;
}
#pragma clang diagnostic pop
@end
