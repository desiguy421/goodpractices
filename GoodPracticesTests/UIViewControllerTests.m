//
//  ViewControllerTests.m
//  GoodPractices
//
//  Created by Aqeel Gunja on 1/16/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <objc/runtime.h>
#import <OCMock/OCMock.h>
#import "TestHelper.h"

void Swizzle(Class class, SEL selector1, SEL selector2)
{
    Method m1 = class_getInstanceMethod(class, selector1);
    Method m2 = class_getInstanceMethod(class, selector2);
    method_exchangeImplementations(m1, m2);
}

@interface UIViewControllerTests : XCTestCase

@property (nonatomic, strong) NSArray *projectViewControllerClasses;


@end

@implementation UIViewControllerTests

- (id)initWithInvocation:(NSInvocation *)anInvocation
{
    if (self = [super initWithInvocation:anInvocation])
    {
        [self populateProjectViewControllerClassList];
    }
    return self;
}

- (void)setUp
{
    [super setUp];
}

- (void)populateProjectViewControllerClassList
{
    NSArray *classes = nil;
    [TestHelper populateProjectClassList:&classes withClassFilterBlock:^BOOL(__unsafe_unretained Class projectClass)
    {
        return (projectClass != [UIViewController class]
                && [projectClass isSubclassOfClass:[UIViewController class]]);
    }];
    self.projectViewControllerClasses = classes;
}

- (void)testAllViewControllersWithBlock:(TestBlock)testBlock
{
    TestBlock block = [testBlock copy];
    for (Class class in self.projectViewControllerClasses)
    {
        block(class);
    }
}


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
- (void)testSuperIsCalledForSelector:(SEL)selector inClass:(Class)class
{
    BOOL didCallSuper = [TestHelper isSuperCalledForSelector:selector inClass:class];
    XCTAssert(didCallSuper, @"[%@ %@] does not call super.", NSStringFromClass(class), NSStringFromSelector(selector));

}
#pragma clang diagnostic pop

- (void)testSuperIsCalledForVariousViewMethods
{
    [self testAllViewControllersWithBlock:^(__unsafe_unretained Class viewControllerClass)
    {
        [self testSuperIsCalledForSelector:@selector(viewDidLoad) inClass:viewControllerClass];
        [self testSuperIsCalledForSelector:@selector(viewWillAppear:) inClass:viewControllerClass];
        [self testSuperIsCalledForSelector:@selector(viewDidAppear:) inClass:viewControllerClass];
        [self testSuperIsCalledForSelector:@selector(viewWillDisappear:) inClass:viewControllerClass];
        [self testSuperIsCalledForSelector:@selector(viewDidDisappear:) inClass:viewControllerClass];
        [self testSuperIsCalledForSelector:@selector(updateViewConstraints) inClass:viewControllerClass];
    }];
}

- (void)testFrameIsNotModifiedInViewDidLoad
{
    [self testAllViewControllersWithBlock:^(__unsafe_unretained Class viewControllerClass)
    {
        UIViewController *viewController = [[viewControllerClass alloc] init];
        id mockView = [OCMockObject niceMockForClass:[UIView class]];
        [[[mockView reject] ignoringNonObjectArgs] setFrame:CGRectNull];
        viewController.view = mockView;
        [viewController viewDidLoad];
        [mockView verify];
    }];
}

- (void)testViewIsNotCalledInInitMethod
{
    [self testAllViewControllersWithBlock:^(__unsafe_unretained Class viewControllerClass)
     {
         NSLog(@"testViewIsNotCalledInInitMethod");
         UIViewController *theViewController = [[viewControllerClass alloc] init];
         XCTAssert(![theViewController isViewLoaded], @"View should not be called in init method");
     }];
}

@end
