//
//  TestHelper.h
//  GoodPractices
//
//  Created by Aqeel Gunja on 1/20/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import <objc/runtime.h>

typedef void(^TestBlock)(Class class);
typedef BOOL(^ClassFilterBlock)(Class projectClass);

@interface TestHelper : NSObject

+ (void)populateProjectClassList:(NSArray **)classArray withClassFilterBlock:(ClassFilterBlock)classFilterBlock;

+ (BOOL)isSuperCalledForSelector:(SEL)selector inClass:(Class)class;

@end
