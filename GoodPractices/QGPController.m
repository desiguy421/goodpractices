//
//  QMCController.m
//  TestProject
//
//  Created by Aqeel Gunja on 1/13/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import "QGPController.h"

@interface QGPController ()

@property (nonatomic, strong) NSMutableArray *things;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, copy) NSString *testString;

@end

@implementation QGPController

- (id)init
{
    if ((self = [super init]))
    {
        // Sample timer just to simulate adding, removing, & replacing data
        _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        _things = [@[@"This", @"is", @"broken", @"now", @"that", @"it's", @"light"] mutableCopy];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        [self performSelector:@selector(swapTwoItems) withObject:nil afterDelay:0.0];
    }
    return self;
}

- (void)swapTwoItems
{
    NSMutableArray *things = [self observableThings];
    [things exchangeObjectAtIndex:1 withObjectAtIndex:3];
}

// Sample timer just to simulate adding, removing, & replacing data
- (void)timerFired:(NSTimer *)timer
{
    int randomInt = arc4random() % 3;
    NSInteger randomIndex = ([self.things count]) ? arc4random() % [self.things count] : 0;
    switch (randomInt) {
        case 0:
        {
            [self.observableThings insertObject:@"Insert" atIndex:randomIndex];
        }
            break;
        case 1:
        {
            if ([self.things count])
            {
                [self.observableThings removeObjectAtIndex:randomIndex];
            }
        }
            break;
        case 2:
        {
            if ([self.things count])
            {
                [self.observableThings replaceObjectAtIndex:randomIndex withObject:@"Replaced"];
            }
        }
            break;
        default:
            break;
    }
}

- (NSMutableArray *)observableThings
{
    return [self mutableArrayValueForKey:@"things"];
}

- (NSUInteger)countOfThings
{
    return [self.things count];
}

- (id)objectInThingsAtIndex:(NSUInteger)index
{
    return [self.things objectAtIndex:index];
}

- (NSArray *)thingsAtIndexes:(NSIndexSet *)indexes
{
    return [self.things objectsAtIndexes:indexes];
}

- (NSInteger)indexOfObjectInThings:(id)object
{
    return [self.things indexOfObject:object];
}

@end
