//
//  QMCAppDelegate.h
//  TestProject
//
//  Created by Aqeel Gunja on 1/13/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QGPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
