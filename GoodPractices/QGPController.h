//
//  QMCController.h
//  TestProject
//
//  Created by Aqeel Gunja on 1/13/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QGPController : NSObject

- (NSUInteger)countOfThings;
- (id)objectInThingsAtIndex:(NSUInteger)index;
- (NSArray *)thingsAtIndexes:(NSIndexSet *)indexes;
- (NSInteger)indexOfObjectInThings:(id)object;

@end
