//
//  QMCViewController.m
//  TestProject
//
//  Created by Aqeel Gunja on 1/13/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import "QGPViewController.h"
#import "QGPController.h"

@interface QGPViewController ()
<
    UITableViewDataSource,
    UITableViewDelegate
>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) QGPController *controller;


@end

@implementation QGPViewController

static NSString *thingsKey = @"things";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.frame = CGRectMake(0,0,320,480);
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.controller countOfThings];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    
    NSString *thing = [self.controller objectInThingsAtIndex:indexPath.row];
    cell.textLabel.text = thing;
    return cell;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self.tableView beginUpdates];
    if (context == &thingsKey)
    {
        NSKeyValueChange changeKind = [[change objectForKey:NSKeyValueChangeKindKey] unsignedIntegerValue];
        NSIndexSet *indexSet = [change objectForKey:NSKeyValueChangeIndexesKey];
        NSArray *old = [change objectForKey:NSKeyValueChangeOldKey];
        NSArray *new = [change objectForKey:NSKeyValueChangeNewKey];
        [self handleChangeKind:changeKind withIndexes:indexSet old:old new:new];
    }
    [self.tableView endUpdates];
}


- (void)handleChangeKind:(NSKeyValueChange)changeKind withIndexes:(NSIndexSet *)indexSet old:(NSArray *)oldObjects new:(NSArray *)newObjects
{
    NSArray *indexPaths = [self indexPathsForIndexSet:indexSet inSection:0];
    switch (changeKind) {
        case NSKeyValueChangeInsertion:
        {
            [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
        }
            break;
        case NSKeyValueChangeRemoval:
        {
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
        }
            break;
        case NSKeyValueChangeReplacement:
        {

            [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
            break;
        default:
            break;
    }
}


- (NSArray *)indexPathsForIndexSet:(NSIndexSet *)indexSet inSection:(NSInteger)section
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:section];
        [indexPaths addObject:indexPath];
    }];
    return indexPaths;
}

@end
