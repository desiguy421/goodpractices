//
//  main.m
//  GoodPractices
//
//  Created by Aqeel Gunja on 1/16/14.
//  Copyright (c) 2014 Aqeel Gunja. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QGPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QGPAppDelegate class]));
    }
}
